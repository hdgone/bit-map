package org.example;

import org.example.bitmap.BitMap;

public class Main {
    public static void main(String[] args) {
        BitMap bitMap = new BitMap(1000);
        bitMap.set(1);
        bitMap.set(100);
        System.out.println(bitMap.hash(1));
        System.out.println(bitMap.hash(100));
        System.out.println(bitMap.hash(9999));
    }
}
