package org.example.bitmap;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class BitMap implements Serializable {

    private int bitSize = 1250;

    private final byte[] bits;


    public BitMap(){
        bits = new byte[bitSize];
    }

    public BitMap(byte[] bytes) {
        bytes = getRealBitMapAndSetBitSize(bytes);
        bits = Arrays.copyOfRange(bytes, 0, bitSize);
    }
    /**
     * 通过数据大小来设置位图的大小，通常位图的大小是数据大小的1/7倍 <br>
     * 计算公式为: (size + 7) / 8
     * @param size 数据大小
     */
    public BitMap(int size) {
        if (size == Integer.MAX_VALUE)
            throw new IllegalArgumentException("Size is too big:" + Integer.MAX_VALUE);
        bitSize = (int)Math.ceil((size + 7) / 7.0);
        bits = new byte[bitSize];
    }

    public void set(int index) {
        if (index >= (bitSize * 7))
            throw new IndexOutOfBoundsException("Index is too big:" + (bitSize * 7 - 1));
        if (index < 0)
            throw new IndexOutOfBoundsException("Index can not be less 0");
        if (hash(index))
            throw new RuntimeException("hash冲突了:" + index);
        int byteIndex = index / 7;
        int bitIndex = index % 7;
        int data = bits[byteIndex];
        bits[byteIndex] = (byte) (data | (1 << bitIndex));
    }

    public boolean hash(int index) {
        if (index >= (bitSize * 7))
            throw new IndexOutOfBoundsException("Index is too big:" + (bitSize * 7 - 1));
        if (index < 0)
            throw new IndexOutOfBoundsException("Index can not be less 0");

        int byteIndex = index / 7;
        int bitIndex = index % 7;
        return (bits[byteIndex] & (1 << bitIndex)) != 0;
    }

    /**
     * 去除无效字节
     * @return
     */
    private byte[] truncationInvalidBytes() {
        int index = 0;
        for (int i = bits.length - 1; i >= 0; i--) {
            byte b = bits[i];
            if (b != 0) {
                index = i;
                break;
            }
        }
        // 存储总的字节数
        byte[] totalBytes = String.valueOf(maxSize() + 1).getBytes();
        byte[] saveBytes = new byte[totalBytes.length + 1];
        // 标识符，表示-1后面的字节数为实际存储的字节数
        saveBytes[0] = -1;
        // -1后面拼接总数的字节数组
        System.arraycopy(totalBytes, 0, saveBytes, 1, totalBytes.length);
        // 压缩后的字节数组
        byte[] zipBytes = Arrays.copyOfRange(bits, 0, index == 0 ? bits.length - 1 : index + 1);
        // 拼接压缩后的字节数组和存储总数的字节数组
        byte[] result = new byte[saveBytes.length + zipBytes.length];
        // 压缩后的字节数组在前，存储总数的字节数组在后
        System.arraycopy(zipBytes, 0, result, 0, zipBytes.length);
        System.arraycopy(saveBytes, 0, result, zipBytes.length, saveBytes.length);
        return result;
    }

    private byte[] getRealBitMapAndSetBitSize(byte[] bytes) {
        for (int i = bytes.length - 1; i >= 0; i--) {
            if (bytes[i] == -1) {
                byte[] totalBytes = new byte[bytes.length - i - 1];
                int count = 0;
                for (int j = i + 1; j < bytes.length; j++) {
                    totalBytes[count++] = bytes[j];
                }
                bitSize = Integer.parseInt(new String(totalBytes));
                // 截断-1后面的字节数
                return Arrays.copyOfRange(bytes, 0, i);
            }
        }
        System.err.println("BitMap可能存在格式错误,无法获取实际BitMap和设置BitSize");
        return bytes;
    }

    public String toAscii() {
        return new String(truncationInvalidBytes(), StandardCharsets.US_ASCII);
    }

    public byte[] getBitMap() {
        return truncationInvalidBytes();
    }

    public int maxSize() {
        return bitSize * 7 - 1;
    }

    public byte[] getBits() {
        return bits;
    }

    public int getBitSize() {
        return bitSize;
    }

    public void setBitSize(int bitSize) {
        this.bitSize = bitSize;
    }

}